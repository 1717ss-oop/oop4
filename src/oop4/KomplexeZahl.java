package oop4;

/**
 * Modelliert eine komplexe Zahl (double,double)
 * @author leo
 */

public class KomplexeZahl {
	private double a; 
	private double b;
	
	/**
	 * gibt Realteil zurueck
	 * @return Realtiel a der Zahl
	 */
	public double getA(){
		return a; 
	}
	
	/**
	 * gibt Imaginaerteil der Zahl zurueck
	 * @return Imaginaerteil
	 */
	public double getB(){
		return b;
	}
	
	/**
	 * erstellt eine neue komplexe Zahl in der Form " c = a+b*i"
	 * @param a Realteil
	 * @param b Imaginaerteil
	 */
	public KomplexeZahl(double a, double b){
		this.a = a; 
		this.b = b; 
	}
}
