/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oop4;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Main: Programm starten, Fallback wenn javaFX nicht unterstuetzt wird.
 * @author leo
 */
public class Oop4 extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
                
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Should be ignored if the application is correctly launced as javafx application.
     * keepig it as a fallback... its just pasing the args and starting the fx application.#
     * fallback is kinda necessary for javafx in netbeans (either netbeans is dumb or i am...)
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // pass the arguments to javafx.application.Application.launch
        launch(args);
    }
    
}
