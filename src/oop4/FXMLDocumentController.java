/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oop4;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelWriter;
import javafx.scene.paint.Color;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.HBox;

/**
 * Controller, verwaltet gui actions, loest berechnungen aus.
 * @author leo
 */
public class FXMLDocumentController implements Initializable {
    
    /**
     * default value: resolution_width = 1000px
     */
    final private int width = 1000;
    /**
     * default value: resolution_height = 1000px
     */
    final private int height = 1000;
    private WritableImage wImage;
    /**
     * set this flag for debugging
     */
    private Boolean debug = false;
    /**
     * default value: State (für die beispielBilder) = 0
     */
    private int state = 0;
    
    @FXML
    private Label lblStatus;
    @FXML
    private ImageView imageView;
    @FXML
    private HBox box;
    @FXML
    private TextField txbCa;
    @FXML
    private TextField txbCb;
    @FXML
    private TextField txbIterationen;
    @FXML
    private Button btnExecute;
            
    
    /*
     * helpful: http://docs.oracle.com/javafx/2/image_ops/jfxpub-image_ops.htm
     */
    
    
    /**
     * eine der gespeicherten Mandelbrotmengen berechnen und anzeigen.
     * @param event ActionEvent (click)
     */
    @FXML
    private void handleBtnDefaults(ActionEvent event) {
        if (state == 0) {
            txbCa.setText("-3-2i");
            txbCb.setText("1+2i");
            txbIterationen.setText("10");
        }else if (state == 1){
            txbCa.setText("-0.3+0.9i");
            txbCb.setText("+0.3+0.1i");
            txbIterationen.setText("80");
        }else if (state == 2){
            //FIXME drittes beispiel muss noch rein.
        	 txbCa.setText("-1.596-0.0002i");
             txbCb.setText("-1.5956+0.0002i");
             txbIterationen.setText("250");
        }
        
        state +=1;
        if (state>2){
            state = 0;
        }
        btnExecute.fire();
    }
    
    /**
     * neue Mandelbrotmenge berechnen und anzeigen.
     * @param event ActionEvent (click)
     */
    @FXML
    private void handleBtnExecuteAction(ActionEvent event) {
        lblStatus.setText("Eingaben prüfen...");     
        
        KomplexeZahl a = getKomplexeZahl(txbCa.getText());
        KomplexeZahl b = getKomplexeZahl(txbCb.getText());
        int tiefe = getTiefe(txbIterationen.getText());
        
        if (a != null && b != null && tiefe > 0){
            berechne(a,b,tiefe);
        }               
    }
    /**
     * Mandelbrotmenge berechnen und Bild anzeigen
     * @param a KomplexeZahl c_a
     * @param b KomplexeZahl c_b
     * @param tiefe Anzahl Iterationen
     */
    private void berechne(KomplexeZahl a, KomplexeZahl b, int tiefe){
        lblStatus.setText("Loading...");
        //reset image
        setDefaultImage(width, height, wImage.getPixelWriter());
        //MVC not required -> let the model draw the pixels directly
        Model model = new Model(a,b, width, height, tiefe, wImage.getPixelWriter());
        model.berechne();
        imageView.setImage(wImage);
        lblStatus.setText("Finished!");
    }
    
    /**
     * String -> Int Conversion mit Validation
     * @param txt String mit positiven Integer
     * @return Integer, -1 falls txt kein positiver Integer war.
     */
    private int getTiefe(String txt){
        Matcher matcher = Pattern.compile("([+]?[0-9]+)").matcher(txt);
        if (!matcher.matches()){
            return -1; //String war kein positiver Integer
        }
        return Integer.parseInt(matcher.group(1));
    }
     
    /**
     * String -> komplexer Zahl Conversion mit Validation
     * @param txt String mit komplexer Zahl
     * @return KomplexeZahl, null falls txt kein String mit komplexer Zahl war
     */
    private KomplexeZahl getKomplexeZahl(String txt){
        //Den regex kann man bestimmt noch schöner hinbekommen... tut aber erstmal was es soll
        String regex_komplexeZ = "([+-]?[0-9]+([.][0-9]+)?){1}([+-]{1}([+-]?[0-9]+([.][0-9]+)?){0,1})i";
        Matcher matcher = Pattern.compile(regex_komplexeZ).matcher(txt);
        if (!matcher.matches()){
            return null; //String war keine komplexe Zahl.
        }
        //this is for debugging only..
        if (matcher.matches() && debug){
            System.out.println("matched!");
            for (int i=0; i <= (int)matcher.groupCount()-1; i++){
                System.out.println("group "+i+": "+matcher.group(i));
            }
        }
        KomplexeZahl result = new KomplexeZahl(Double.parseDouble(
                matcher.group(1)),
                (matcher.group(3).equals("+")) ? 1.0 : (matcher.group(3).equals("-")) ? -1.0 : Double.parseDouble(matcher.group(3))
        );
        return result;
    }
    /**
     * reset the image (setze jeden pixel auf schwarz)
     * @param width hoehe
     * @param height breite
     * @param pw pixelwriter des images
     */
    private void setDefaultImage(int width, int height, PixelWriter pw){
        // Create a black Image to start with the color of each pixel in a specified row
        for(int readY=0;readY<height;readY++){
            for(int readX=0; readX<width;readX++){
                Color color = Color.BLACK;
                pw.setColor(readX,readY,color);
            }
        }
    }
    
    /**
     * initialize the view, create the image, bind the imageviewsize for resizing
     * @param url url
     * @param rb ResourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        // Create WritableImage
         wImage = new WritableImage(
                 (int)width,
                 (int)height);       
        imageView.setImage(wImage);
        //scale the imageView with its image if the hbox is resized.
        imageView.fitWidthProperty().bind(box.widthProperty());
        imageView.fitHeightProperty().bind(box.heightProperty());
        lblStatus.setText("Ready!");
    }    
    
}
