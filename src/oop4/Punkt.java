package oop4;

/**
 * Modelliert einen Punkt mit x,y Koordinate und tiefe.
 * @author Greta
 *
 */
public class Punkt extends KomplexeZahl {
	private int tiefe;
	
	/**
	 * erstellt einen neuen Punkt
	 * @param a  Realteil, also x-wert
	 * @param b	 Imaginaerteil, also y-wert
	 * @param tiefe Iterationstiefe in der festgestellt wurde, dass der Punkt nicht mehr zur menge gehoert
	 */
	Punkt(double a, double b, int tiefe){
		super(a,b);
		this.tiefe = tiefe;
	}
	
	/**
	 * git Iterationstiefe zurueck
	 * @return Tiefe
	 */
	public int getTiefe(){
		return tiefe;
	}
	
}
