package oop4;

import javafx.scene.image.PixelWriter;
import javafx.scene.paint.Color;

/**
 * mathematische Berechnungen zur Mandelbrotmenge
 * @author Greta
 *
 */
public class Model {
	private KomplexeZahl a; 
	private KomplexeZahl e;
    int height, width, tiefe;
    PixelWriter pw;
	//private Gui GUI; 
	
    /**
     * erstellt ein neues Model zum berechnen, welches das Koordinatensystem 
     * im Bereich [Ca, Ce] auf den Bildschirm (width, height) anpasst, und die Mandelbrotmenge dazu berechnet
     * @param a Begrenzungseckpunkt Ca,
     * @param e Begrenzungseckpunkt Ce, 
     * @param width Breite des Bildschirms (Anzahl Pixel)
     * @param height Hoehe des Bildschirms (Anzahl Pixel)
     * @param tiefe Maximale Iterationstiefe die geprueft wird, default oder vom nutzer eingestellt
     * @param pw verwendeter Pixelwriter
     */
	public Model(KomplexeZahl a, KomplexeZahl e, int width, int height, int tiefe, PixelWriter pw){
		this.a = a; 
		this.e = e;
        this.height = height;
        this.width = width;
        this.tiefe = tiefe;
        this.pw = pw;
	}
	
	/**
	 * fuegt den uebergebenen Punkt in die Grafik ein. Seine Faerbung wird abhaengig von der Iterationstiefe bestimmt.
	 * @param p einzuzeichnender Punkt
	 */
	public void addPunkt(Punkt p){
            int x = (int)p.getA();
            int y = (int)p.getB();
            if (0 <= x && x < width && 0 <= y && y < height){
                pw.setColor(x,y,Color.rgb(255-Math.round(255/tiefe * p.getTiefe()), Math.round(255/tiefe * p.getTiefe()) , Math.round(255/tiefe * p.getTiefe())));
                //pw.setColor(x,y,Color.rgb(255, 255, Math.round(255/tiefe * p.getTiefe())));
            }else{
                //FIXME shoudlnt happen
                System.out.println("("+x+","+y+") = "+p.getTiefe());
            }
            
	}
    
	/**
	 * Berechnet fuer die uebergebene Zahl c die Iterationstiefe in der klar wird,
	 * ob z fuer c endlich bleibt oder nicht
	 * @param c zu testende komplexe Zahl
	 * @return Tiefe in der erstmals |z| groesser 4 ist, also z unendlich wird, ansonsten -1 wenn z bis zur max. Itiefe endlich bleibt.
	 *
	 */
	public int berechneTiefe(KomplexeZahl c){
		double x= 0;
		double y= 0;
		double xVG = 0; 
		double yVG = 0; 
		
		for(int i=1; i<tiefe; i++){
			x = (xVG*xVG-yVG*yVG+c.getA());
			y = (2*xVG*yVG+c.getB());
			
			if (x*x+y*y > 4) return i;
			xVG = x; 
			yVG = y;
		}
		return -1;
	}
	
	/**
	 * berechnet fuer jeden Punkt im Bildschirm, entsprechend des Koordinatensystems,
	 * ob der Punkt zur Menge gehoert oder nicht, und zeichnet ihn ggf, mit seiner entsprechnenden Farbe ein.
	 */
	public void berechne(){
		for(int i = 0; i<height; i++){
			for (int j = 0; j<width;j++){
				double ca= (((e.getA()-a.getA())/width)*j+a.getA());
				double cb= (((e.getB()-a.getB())/height)*i+a.getB());
				KomplexeZahl c = new KomplexeZahl(ca,cb);
				int t = berechneTiefe(c);
				if (t !=-1){
				Punkt p = new Punkt(j,i,t);
					addPunkt(p);
				}
			}
		}
	}
}
